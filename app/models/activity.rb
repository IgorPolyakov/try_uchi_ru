# frozen_string_literal: true

require 'uri'
require 'prawn'
require 'zip'
require 'digest'
require 'redis/objects'

class ValidateUrlError < StandardError
end

class RecordNotFound < StandardError
end

class ContributorNotFound < StandardError
end

class ResponseIsEmpty < StandardError
end

# class representing the layer between the application and the api github
class Activity
  include HTTParty
  include Redis::Objects
  attr_reader :url
  attr_reader :data
  attr_reader :id
  attr_reader :files

  NUMBER_OF_CONTRIBUTORS = 3
  MAX_RETRIES = 3

  def initialize(params)
    url = params[:url]
    @url = url
    @id = "activity:#{Digest::MD5.hexdigest url}"
    @data = params[:data]
    @files = {
      diploma: "public/diploma_#{@id}",
      archive: "public/archive_#{@id}.zip"
    }
  end

  def save
    redis.set(@id, Marshal.dump(self))
  end

  def self.cache_public_contributors(url)
    Rails.cache.fetch([url, __method__], expires_in: 30.minutes) do
      public_contributors(url)
    end
  end

  def self.public_contributors(url)
    validate_url(url)
    param = parse(url)
    api_url = "https://api.github.com/repos/#{param}/stats/contributors"
    select_contributors(try_take_contributors(api_url))
  end

  def self.try_take_contributors(api_url)
    response = HTTParty.get(api_url)
    raise ResponseIsEmpty, 'data is empty' if response.parsed_response.empty?

    response.parsed_response
  rescue ResponseIsEmpty => error
    @retries ||= 0
    raise error if @retries > MAX_RETRIES

    @retries += 1
    retry
  end

  def self.validate_url(url)
    unless url.match?(URI::DEFAULT_PARSER.make_regexp)
      raise ValidateUrlError, 'its not url'
    end
    raise ValidateUrlError, 'only github' unless url.include? 'github.com'
  end

  def self.parse(str)
    str.split('/').last(2).join('/')
  end

  def self.select_contributors(contributors)
    list_of = contributors.last(NUMBER_OF_CONTRIBUTORS)
    list_of.each do |contributor|
      contributor.delete('weeks')
    end
    list_of.reverse
  end

  def diploma(top)
    raise ContributorNotFound, 'contributor not found' unless data[top]

    name = data.dig(top, 'author', 'login')
    file_name = "#{@files[:diploma]}_#{name}.pdf"

    return file_name if File.exist? file_name

    total = data.dig(top, 'total')
    generate_pdf(name, total, top, file_name)
  end

  def create_zip
    zipfile_name = @files[:archive]
    return zipfile_name if File.exist? zipfile_name

    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      (0...NUMBER_OF_CONTRIBUTORS).each do |index|
        file_name = diploma(index)
        zipfile.add(file_name, File.join('./', file_name))
      end
    end
    zipfile_name
  end

  def self.find(id)
    result = redis.get id
    raise RecordNotFound, 'Sorry, record does not exist' unless result

    Marshal.load result
  end

  def self.all
    redis.keys('activity:*').collect { |key| find(key) }
  end

  private

  def generate_pdf(name, total, top, file_name)
    pdf = Prawn::Document.new
    pdf.move_down 50
    pdf.text("##{top + 1}", align: :center, size: 72)
    pdf.text(name.to_s, align: :center, size: 44)
    pdf.text(total.to_s, align: :center, size: 14)
    pdf.render_file file_name
    file_name
  end
end
