# frozen_string_literal: true

# controller processes all requests related to the activity
class ActivitiesController < ApplicationController
  before_action :set_activity, only: %i[show download_zip download_pdf]

  def index
    @activities = Activity.all
  end

  def show
    @url = @activity.url
    @show = @activity.data
  end

  def create
    url = activity_params[:url]
    data = Activity.cache_public_contributors(url)
    @activity = Activity.new(url: url, data: data)
    @activity.save
    redirect_to activity_path(id: @activity.id)
  rescue StandardError => e
    redirect_to root_path, alert: e.message
  end

  def download_zip
    name = @activity.create_zip
    send_file name, disposition: 'attachment'
  rescue Errno::ENOENT
    redirect_to root_path, alert: 'zip not found'
  rescue ContributorNotFound => error
    redirect_to root_path, alert: error.message
  end

  def download_pdf
    name = @activity.diploma(params[:top].to_i)
    send_file name, disposition: 'attachment'
  rescue Errno::ENOENT
    redirect_to root_path, alert: 'pdf not found'
  rescue ContributorNotFound => error
    redirect_to root_path, alert: error.message
  end

  private

  def set_activity
    @activity = Activity.find(params[:id])
  rescue RecordNotFound => error
    redirect_to root_path, alert: error.message
  end

  def activity_params
    params.require(:activity).permit(:url)
  end
end
