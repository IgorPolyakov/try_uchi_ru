# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap', '~> 4.1'
gem 'httparty', '~> 0.16.2'
gem 'jquery-rails', '~> 4.3'
gem 'prawn', '~> 2.2'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.1'
gem 'redis-objects', '~> 1.4'
gem 'redis-rails', '~> 5.0'
gem 'rubyzip', '~> 1.2'
gem 'sass-rails', '~> 5.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'rspec-rails'
  gem 'simplecov', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'pry-rails', '~> 0.3.6'
  gem 'rubocop', '~> 0.59.2'
  gem 'rubycritic', '~> 3.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
