# frozen_string_literal: true

Redis.current = if Rails.env.test?
                  Redis.new(url: 'redis://redis:6379')
                elsif Rails.env.production?
                  Redis.new(url: 'redis://46.17.43.85:6379')
                else
                  Redis.new(url: ENV['REDIS_URL'] || 'redis://localhost:6379')
                end
