# frozen_string_literal: true

Rails.application.routes.draw do
  root 'activities#index'
  resources :activities, only: %i[index show create download_zip download_pdf] do
    member do
      get :download_zip
      get :download_pdf
    end
  end
end
