# Тестовое задание для Ruby On Rails разработчика

[![pipeline status](https://gitlab.com/IgorPolyakov/try_uchi_ru/badges/master/pipeline.svg)](https://gitlab.com/IgorPolyakov/try_uchi_ru/commits/master)
![coverage](https://gitlab.com/IgorPolyakov/try_uchi_ru/badges/master/coverage.svg)

[demo](https://try-uchi-ru.herokuapp.com)

Мы в `******` пишем на руби и нам часто приходится иметь дело с обработкой JSON.

Ещё мы проводим онлайн-олимпиады и генерируем pdf-дипломы на лету.

Поэтому наше задание про это.

Нужно по адресу публичного репозитория на гитхабе найти трёх его самых активных

контрибьюторов и сгенерировать для них простые pdf-дипломы и архив с ними.

Спроектируйте приложение, напишите тесты проверяющие его работоспособность, и разверните его на любой публичной платформе (например, на бесплатном аккаунте в Хероку).

Код, конечно же, должен быть на Гитхабе.

Полезные ссылки, которые могут пригодится:

Git:
*   [git-scm.com](https://git-scm.com/book/en/v2)
*   [proglib.io](https://proglib.io/p/git-for-half-an-hour)
*   [gitimmersion.com](http://gitimmersion.com/index.html)
*   [learngitbranching.js.org](https://learngitbranching.js.org)
*   [try.github.io](https://try.github.io/levels/1/challenges/3)

Docker:
*   [www.docker.com](https://www.docker.com)
*   [habrahabr.ru](https://habrahabr.ru/post/253877)
*   [rus-linux.net](http://rus-linux.net/MyLDP/vm/docker/docker-tutorial.html)
*   [proglib.io](https://proglib.io/p/docker)
*   [habrahabr.ru](https://habrahabr.ru/post/310460)

heroku:
*   [blog.heroku.com](https://blog.heroku.com/container-registry-and-runtime)
*   [devcenter.heroku.com](https://devcenter.heroku.com/articles/container-registry-and-runtime)
