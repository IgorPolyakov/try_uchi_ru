# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ActivitiesController, type: :controller do
  describe 'GET index page' do
    it 'index page has a 200 status code' do
      get :index
      expect(response.content_type).to eq 'text/html'
      expect(response.status).to eq(200)
    end

    it 'create activity' do
      url = 'https://github.com/rails/rails'
      post :create, params: { activity: { url: url } }
      activity = Activity.find('activity:c1697eee13b5b7712c2814f65b21e36a')
      expect(activity).to be_kind_of(Activity)
      expect(response.content_type).to eq 'text/html'
      expect(response.status).to eq(302)
    end

    # %i[download_zip download_pdf show].each do |page|
    #   url = 'https://github.com/rails/rails'
    #   post :create, params: { activity: { url:  url } }
    #   activity = Activity.find('activity:c1697eee13b5b7712c2814f65b21e36a')
    #   it "#{page} page has a 200 status code" do
    #     get page id: activity.id
    #     # expect(response.content_type).to eq 'text/html'
    #     expect(response.status).to eq(200)
    #   end
    # end
  end
end
