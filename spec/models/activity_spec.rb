# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Activity, type: :model do
  it 'raise with invalid attributes' do
    expect do
      Activity.cache_public_contributors('')
    end.to raise_error(ValidateUrlError)
  end
  it 'raise with gitlab url' do
    url = 'https://gitlab.com/gitlab-org/gitlab-runner'
    expect do
      Activity.cache_public_contributors(url)
    end.to raise_error(ValidateUrlError)
  end
  it 'get params from url' do
    url = 'https://github.com/rails/rails'
    expect(Activity.parse(url)).to eq('rails/rails')
  end
  it 'return json' do
    url = 'https://github.com/rails/rails'
    expect(Activity.cache_public_contributors(url)).to be_kind_of(Array)
  end

  it 'get last contributors from github repo' do
    url = 'https://github.com/rails/rails'
    data = Activity.cache_public_contributors(url)
    Activity.new(url: url, data: data).save
    expect(Activity.all).to be_kind_of(Array)
  end
end
